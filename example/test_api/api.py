import dataclasses
from typing import List

from django.contrib.auth.models import Permission
from django_fastapi import get_default_api
from django_fastapi.shortcuts import render
from fastapi import Request
from fastapi.responses import HTMLResponse

api = get_default_api()


@dataclasses.dataclass
class HelloWorld:
    hello: str
    foo: str
    another_resource: str
    docs: List[str] = dataclasses.field(
        default_factory=lambda: ["/docs/", "/redoc/"])


@dataclasses.dataclass
class AnotherResource:
    another: str


@dataclasses.dataclass
class PermissionListResource:
    items: List


@api.get("/", response_model=HelloWorld)
def serve_root():
    return HelloWorld(hello="world", foo="bar", another_resource="/another/")


@api.get("/another/", response_model=AnotherResource)
def another_resource():
    return AnotherResource(another="resource")


@api.get("/permissions", response_model=PermissionListResource)
def permissions_list():
    queryset = Permission.objects.all()
    items = [{"pk": obj.pk, "name": obj.name} for obj in queryset]
    return PermissionListResource(items=items)


@api.get("/async/permissions", response_model=PermissionListResource)
async def async_permissions_list():
    queryset = Permission.objects.all()
    items = [{"pk": obj.pk, "name": obj.name} async for obj in queryset]
    return PermissionListResource(items=items)


@api.get("/template", response_class=HTMLResponse)
def django_template(request: Request):
    return render(request, "test_template.html")


@api.get("/async/template", response_class=HTMLResponse)
async def sync_django_template(request: Request):
    return render(request, "test_template.html")


@api.get("/exception")
def exception(request: Request):
    raise RuntimeError("boo!")


@api.get("/async/exception")
async def async_exception(request: Request):
    raise RuntimeError("boo!")
